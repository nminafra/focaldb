<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>FOCAL EQUIPMENT DB</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Sonsie+One" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css">

    <!-- the below three lines are a fix to get HTML5 semantic elements working in old versions of Internet Explorer-->
    <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ff0000">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body>
    <!-- Here is our main header that is used across all the pages of our website -->

    <header>
      <h1>ALICE FoCal parts</h1>
      <!-- <h3><a href="qr.php">Scan QR ID</a></h3> -->
    </header>

    <!-- <nav>
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">Our team</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>


       <form>
         <input type="search" name="q" placeholder="Search query">
         <input type="submit" value="Go!">
       </form>
     </nav> -->

    <main>
        <div class="container">
            <div class="item">
                <form method="post" action="">
                    <h2>Add a new Equipment: </h2>
                    <div>
                    <p>
                    <label for="identification">Identification*:</label>
                    <a href="qr.php"><img src="qricon.png" alt="Scan" style="height:1em"></a>
                    <br/>
                    <?php 
                        if ( isset($_GET['identification']))
                            echo '<input type="text" name="identification" value=' . $_GET['identification'] . ' ></br>';
                        else
                            echo '<input type="text" name="identification" placeholder="W1_1" required></br>';
                    ?>
                    <input type="checkbox" id="update" name="update" value="performUpdate">
                    <label for="update"> Update existent entry? </label><br/>

                    <label for="category">Category:</label><br/>
                    <select id="category" name="category">
                        <option value="sensor">Sensor</option>
                        <option value="labequipment">Lab Equipment</option>
                        <option value="itequipment">IT Equipment</option>
                        <option value="electronics">Electronics</option>
                        <option value="tooling">Tooling</option>
                    </select><br />
                    <label for="description">Description*: </label><br/>
                    <input type="text" name="description" placeholder="Details of the Equipment" required/><br />
                    <label for="manufacturer">Manufacturer: </label><br/>
                    <input type="text" name="manufacturer" placeholder="Who made it?" /><br />
                    <label for="owner">Owner*: </label><br/>
                    <input type="text" name="owner" placeholder="Who paid?" required/><br />
                    <label for="location">Location*: </label><br/>
                    <input type="text" name="location" placeholder="where are you?" required/><br />
                    <label for="responsible">Responsibile*: </label><br/>
                    <input type="text" name="responsible" placeholder="your name" required/><br />
                    <label for="email">Email*: </label><br/>
                    <input type="email" name="email" placeholder="Enter Your Email" required/><br />
                    <label for="quantity">Quantity: </label><br/>
                    <input type="number" name="quantity" placeholder="1"/><br />
                    <label for="notes">Notes:</label><br/>
                    <textarea name="notes" rows="10" cols="30" maxlength="250"></textarea>
                    </p>
                    </div>

                    <button type="submit" name="addForm" value="Submit">Submit</button>
                    <button type="reset" value="Reset">Reset</button>
                </form>

            </div>
            <div class="item">

                <form method="post" action="" name="actionForm" enctype="multipart/form-data">
                    <h2>Add a new Action:</h2>
                    <div>
                    <p>
                    <label for="identification">Identification*:</label>
                    <a href="qr.php"><img src="qricon.png" alt="Scan" style="height:1em"></a>
                    <br/>
                    <?php 
                        if ( isset($_GET['identification']))
                            echo '<input type="text" name="identification" value=' . $_GET['identification'] . ' ></br>';
                        else
                            echo '<input type="text" name="identification" placeholder="W1_1" required></br>';
                    ?>

                    <label for="description">Type:</label><br/>
                    <select id="description" name="description">
                        <option value="picture">Picture</option>
                        <option value="rp">RP</option>
                        <option value="calibration">Calibration</option>
                        <option value="test">Test</option>
                        <option value="other">Other</option>
                    </select>
                    <br/>

                    <label for="location">Location*: </label><br/>
                    <input type="text" name="location" placeholder="where are you?" required/><br />
                    <label for="responsible">Responsibile*: </label><br/>
                    <input type="text" name="responsible" placeholder="your name" required/><br />
                    <label for="email">Email*: </label><br/>
                    <input type="email" name="email" placeholder="Enter Your Email" required/><br />

                    <label for="notes">Notes:</label><br/>
                    <textarea name="notes" rows="10" cols="30" maxlength="250"></textarea><br/>
                    
                    <label for="resultsFile">Upload a file:</label><br/>
                    <input type="file" id="resultsFile" name="resultsFile">
                    </div>
                    <br/>

                    <button type="submit" name="measurementForm" value="Submit">Submit</button>
                    <button type="reset" value="Reset">Reset</button>
                </form>
            </div>
            <div class="item">
                <h2>Explore db:</h2>  
                <form action="" method="post">
                <p> 
                <label for="searchType">db:</label>
                    <select id="searchType" name="searchType">
                        <option value="equipment">Equipment</option>
                        <option value="actions">Actions</option>
                    </select>  
                </p>
                <p> 
                <label for="actionType">Measurement type:</label>
                    <select id="actionType" name="actionType">
                        <option value="picture">Picture</option>
                        <option value="rp">RP</option>
                        <option value="calibration">Calibration</option>
                        <option value="test">Test</option>
                        <option value="other">Other</option>
                    </select>  
                </p>
                <p>
                <label for="idText">Id: </label>
                <a href="qr.php"><img src="qricon.png" alt="Scan" style="height:1em"></a>
                <?php 
                        if ( isset($_GET['identification']))
                            echo '<input type="idText" name="searchId" value=' . $_GET['identification'] . '>';
                        else
                            echo '<input type="idText" name="searchId" value="%" >';
                    ?>
                </p>  
                <p> 
                <label for="locationText">Where: </label>
                <input type="locationText" name="searchLoc" value="%">
                </p> 
                <p> 
                <label for="ownerText">Owner: </label>
                <input type="ownerText" name="searchOwner" value="%">
                </p>  
                <p>          
                <button type="submit" name="searchForm" value="Search">Search</button>
                </p>
                
                <div class="scrollitem" contenteditable="true" style="overflow-y: scroll; width:300px; height:600px; background:white; font-size: 10pt"><?php
                    if (isset($_POST['addForm']) or  isset($_POST['measurementForm']) or isset($_POST['searchForm'])) {
                        // $servername = "188.185.112.86"; //dbod-focalmaterial.cern.ch
                        // $username = "phpuser";
                        // $password = "F0c4lMaterial";
                        // $port = 5503;
                        // $dbname = "focal";
                        $servername = getenv('MYSQL_SERVICE_HOST');
                        $username = getenv('MYSQL_USER');
                        $password = getenv('MYSQL_PASSWORD');
                        $port = getenv('MYSQL_SERVICE_PORT');
                        $dbname = getenv('MYSQL_DATABASE');
                        
                        // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname, $port);

                        if($conn->connect_error){
                            echo 'Connection Faild: '.$conn->connect_error;
                            }
                        else
                        {
                            if (isset($_POST['addForm']))
                            {
                                $identification = $_POST['identification'];
                                $identification = str_replace(' ', '', $identification);
                                $sql="SELECT * FROM equipment WHERE identification LIKE '" . $identification ."';";
                                $result = $conn->query($sql);
                    
                                if ($result->num_rows == 0 or $_POST['update'] == 'performUpdate' ) 
                                {
                                   if ($stmt = $conn->prepare("INSERT INTO equipment (identification, category, description, manufacturer, owner, location, responsible, email, quantity, notes, lastmod) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())")) {
                                        $stmt->bind_param("ssssssssis",$identification, $category, $description, $manufacturer, $owner, $location, $responsible, $email, $quantity, $notes);
                                        // var_dump($stmt)
                                        // set parameters and execute
                                        $identification = $_POST['identification'];
                                        $identification = str_replace(' ', '', $identification);
                                        $category = $_POST['category'];
                                        $description = $_POST['description'];
                                        $manufacturer = $_POST['manufacturer'];
                                        $owner = $_POST['owner'];
                                        $location = $_POST['location'];
                                        $responsible = $_POST['responsible'];
                                        $email = $_POST['email'];
                                        $quantity = $_POST['quantity'];
                                        $notes = $_POST['notes'];
                                        $stmt->execute();
                                        $stmt->close();
                                        echo "Entry inserted correctly </br>";
                                }
                                else 
                                {
                                         echo "Probelm with db </br>:";
                                         echo $stmt->error;
                                         echo "</br>:";
                                }
                                }
                                else
                                {
                                    echo $identification . ' was already present! Check Update if you want to update its record </br>';
                                }

                            }
                            else if (isset($_POST['actionsForm']))
                            {
                                $identification = $_POST['identification'];
                                $identification = str_replace(' ', '', $identification);
                                $sql="SELECT * FROM equipment WHERE identification LIKE '" . $identification ."';";
                                $result = $conn->query($sql);
                    
                                if ($result->num_rows > 0)
                                {
                                    $stmt = $conn->prepare("INSERT INTO actions (identification, description, location, responsible, email, notes, filename, filetype, filesize, data, lastmod) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())");
                                    $stmt->bind_param("sssssssssb",$identification, $description, $location, $responsible, $email, $notes, $filename, $filetype, $filesize, $null);
                                    // set parameters and execute
                                    $identification = $_POST['identification'];
                                    $identification = str_replace(' ', '', $identification);
                                    $description = $_POST['description'];
                                    $location = $_POST['location'];
                                    $responsible = $_POST['responsible'];
                                    $email = $_POST['email'];
                                    $notes = $_POST['notes'];

                                    if (is_uploaded_file($_FILES['resultsFile']['tmp_name']) and $_FILES['upfile']['size'] < 1000000) 
                                    {
                                        $filenameTMP = $_FILES['resultsFile']['name'];
                                        $ext = pathinfo($filenameTMP, PATHINFO_EXTENSION);

                                        if ($ext=="txt"||$ext=="json"||$ext=="dat"||$ext=="csv")
                                        {
                                            $filename = $filenameTMP;
                                            $tmpname = $_FILES['resultsFile']['tmp_name'];
                                            $filesize = $_FILES['resultsFile']['size'];
                                            $filetype = $_FILES['resultsFile']['type'];

                                            $fp      = fopen($tmpname, 'r');
                                            $content = fread($fp, filesize($tmpname));
                                            $content = addslashes($content);
                                            fclose($fp);
                                            $stmt->send_long_data(9,$content);

                                            echo "File included successfully </br>";
                                        }
                                        else if ($ext=="png"||$ext=="PNG"||$ext=="JPG"||$ext=="jpg"||$ext=="jpeg"||$ext=="JPEG")
                                        {
                                            $filename = $filenameTMP;
                                            $tmpname = $_FILES['resultsFile']['tmp_name'];
                                            $filesize = $_FILES['resultsFile']['size'];
                                            $filetype = $_FILES['resultsFile']['type'];

                                            $fp      = fopen($tmpname, 'r');
                                            $content = fread($fp, filesize($tmpname));
                                            // $content = addslashes($content);
                                            fclose($fp);
                                            $stmt->send_long_data(9,$content);

                                            echo "File included successfully </br>";
                                        }
                                        else {
                                            echo "Problem with the file: " . $filenameTMP . "</br>";
                                        }

                                        
                                        
                                    }

                                    if ($stmt->execute())
                                    {
                                        echo "Entry inserted correctly </br>";
                                    }
                                    else
                                    {
                                        echo "Probelm with db </br>:";
                                        echo $stmt->error;
                                        echo "</br>:";
                                    }
                                    $stmt->close();

                                    echo "Measurement added! </br>";
                                }
                                else {
                                    echo "No " . $identification . " was found!";
                                } 
                                
                            }
                            else if (isset($_POST['searchForm']))
                            {
                                $searchId = trim($_POST['searchId']);
                                // echo $searchId . '</br>';
                                $searchLoc = $_POST['searchLoc'];
                                $searchOwner = $_POST['searchOwner'];
                                $additionalSelection = "";
                                // echo $_POST['searchType'] . '</br>';
                                if ($_POST['searchType'] == 'actions')
                                    $additionalSelection = " AND type LIKE '" . $_POST['actionType'] . "'";
                                if ($_POST['searchType'] == 'equipment')
                                    $additionalSelection = " AND owner LIKE '" . $searchOwner . "'";
                                $sql="SELECT * FROM " . $_POST['searchType'] . " WHERE identification LIKE '" . $searchId ."' AND location LIKE '". $searchLoc . "'". $additionalSelection .";";
                                // echo $sql . '</br>';
                                if ($_POST['searchType'] == 'actions')
                                    echo "Looking for ". $_POST['actionType'] . " results for '"   . $searchId . "' at '" . $searchLoc . "': </br>"; 
                                else
                                    echo "Looking for '". $searchId . "' at '" . $searchLoc . "': </br>";  
                                $result = $conn->query($sql);

                                
                                // echo $result->num_rows . '</br></br>';
                    
                                if ($result->num_rows > 0)
                                {
                                    // output data of each row
                                    while($row = $result->fetch_assoc())
                                    {
                                        if ($_POST['searchType'] == 'equipment')
                                        {
                                            echo $row["id"]. ": ";
                                            echo $row["identification"]. " is a ";
                                            if ($row['quantity']>1) 
                                                echo 'box of ' . $row['quantity'] . ' ';
                                            echo $row["description"]. " produced by " . $row["manufacturer"]. ", owned by " . $row["owner"].": at " . $row["location"]. " under the responsibility of " . $row["responsible"]. " [" . $row["email"]. "] ". "</br>";
                                            echo "Last modification: " . $row["lastmod"] . "</br>";
                                            if (isset($row["notes"]))
                                                echo "Notes: " . $row["notes"]  . "</br>";
                                        }
                                        else
                                        {
                                            echo $row["id"]. ": " ;
                                            echo $row["identification"]. ": " . $row["description"] . " performed on " . $row["identification"]. ": at " . $row["location"]. " by " . $row["responsible"]. " [" . $row["email"]. "] ";
                                            if (isset($row["filename"])) {
                                                echo "Attached file: " . $row["filename"] . " of " . $row["filesize"] . " KB". "</br>";
                                                echo '<div contentEditable="false"><a href="https://etldb.web.cern.ch/retrieveFile.php?id='. $row["id"] .'" target="_blank">Download</a></div>';
                                            }
                                            echo "Last modification: " . $row["lastmod"] . "</br>";
                                            if (isset($row["notes"]))
                                                echo "Notes: " . $row["notes"]  . "</br>";
                                        }
                                            echo "</br> </br>";
                                    }
                                }
                                else {
                                    echo "No results </br>";
                                }    
                            }



                            // $sql = "SELECT * FROM dummies";
                            // $result = $conn->query($sql);
                            
                            // if ($result->num_rows > 0) {
                            //   // output data of each row
                            //   while($row = $result->fetch_assoc()) {
                            //     echo "id: " . $row["id"]. " - " . $row["identification"]. " is a " . $row["type"]. " produced by " . $row["manufacturer"]. ": at " . $row["location"]. " under the responsibility of " . $row["responsible"]. " [" . $row["email"]. "] </br>";
                            //   }
                            // } else {
                            //   echo "0 results </br>";
                            // }

                            // echo ' </br>';

                            
                        }
                        // ob_clean();
                        // flush();
                        $conn->close();
                    }
                ?></div>

                </form>
            </div>
        </div>  
    </main>

    <!-- <footer>
      <p>Created by Nicola</p>
    </footer> -->

  </body>
</html>



