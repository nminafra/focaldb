<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>FOCAL EQUIPMENT DB</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Sonsie+One" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ff0000">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body>
    <main>
        <div id="qr-reader" style="width:500px"></div>
        <div id="qr-reader-results"></div>
    </main>
  </body>
  <script src="https://unpkg.com/html5-qrcode"></script>
  <script>
    function docReady(fn) {
        // see if DOM is already available
        if (document.readyState === "complete"
            || document.readyState === "interactive") {
            // call on next available tick
            setTimeout(fn, 1);
        } else {
            document.addEventListener("DOMContentLoaded", fn);
        }
    }

    docReady(function () {
        var resultContainer = document.getElementById('qr-reader-results');

        function onScanSuccess(decodedText, decodedResult) {
            var format = /[ `!@#$%^&*()+\-=\[\]{};':"\\|,.<>\/?~]/;
            if (!format.test(decodedText)) {
                html5QrcodeScanner.clear();
                window.location.href = document.referrer.split("?")[0] + '?identification=' + decodedText;                
            }
        }

        var html5QrcodeScanner = new Html5QrcodeScanner(
            "qr-reader", { fps: 10, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);
    });
  </script>
</html>



