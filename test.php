<?php

/**
 *
 * Copyright MITRE 2012
 *
 * OpenIDConnectClient for PHP5
 * Author: Michael Jett <mjett@mitre.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */


 function convert_object_to_array($data) {
         if(is_object($data)) {
                 // Get the properties of the given object
                 $data = get_object_vars($data);
         }
         if(is_array($data)) {
                 //Return array converted to object
                 return array_map(__FUNCTION__, $data);
         }
         else {
                 // Return array
                 return $data;
         }
 }
 
composer require jumbojett/openid-connect-php
use Jumbojett\OpenIDConnectClient;




$client_id = getenv('CLIENT_ID');
$client_secret = getenv('CLIENT_SECRET');

$oidc = new OpenIDConnectClient(
  'https://auth.cern.ch/auth/realms/cern',
  $client_id,
  $client_secret
);

$oidc->providerConfigParam(array('token_endpoint'=>'https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token'));

// telling to the platform what features they should receive. We give some parameters such as  $client_id, $client_secret and set the scope (addScope) which are needed to tell which data to retrieve
$oidc->addScope('profile email');
// here to authenticate
$oidc->authenticate();

session_start();
// "here you get a token and that token has certain info such as the email/roles....
$idtoken = $oidc->getIdTokenPayload();

$idtoken_array = convert_object_to_array($idtoken);
$userEmail = $idtoken_array["email"];
$_SESSION['userEmail'] = $userEmail;

$roles = $idtoken_array["resource_access"]["dev-test-beam-facilities"]["roles"];

$isAdmin = 0;
$isfcoord = 0;

foreach ($roles as $value){
  if ($value=="facilities-admin"){
    $isAdmin = 1;
  }

  if ($value=="facilities-coordinators"){
    $fcoord = 1;
  }
}

$_SESSION['isAdmin'] = $isAdmin;
$_SESSION['f_coordinator'] = $fcoord;


?>