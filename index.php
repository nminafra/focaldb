<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>FOCAL EQUIPMENT DB</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Sonsie+One" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css">

    <!-- the below three lines are a fix to get HTML5 semantic elements working in old versions of Internet Explorer-->
    <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ff0000">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body>
    <header>
      <h1>ALICE FoCal parts</h1>
      <!-- <h3><a href="qr.php">Scan QR ID</a></h3> -->
    </header>


    <main>
        <div class="container">
            <div class="item">
                <h2>Explore db:</h2>  
                <form action="" method="post">
                <p> 
                <label for="searchType">db:</label>
                    <select id="searchType" name="searchType">
                        <option value="equipment">Equipment</option>
                        <option value="actions">Actions</option>
                    </select>  
                </p>
                <p> 
                <label for="actionType">Measurement type:</label>
                    <select id="actionType" name="actionType">
                        <option value="picture">Picture</option>
                        <option value="rp">RP</option>
                        <option value="calibration">Calibration</option>
                        <option value="test">Test</option>
                        <option value="other">Other</option>
                    </select>  
                </p>
                <p>
                <label for="idText">Id: </label>
                <?php 
                        if ( isset($_GET['identification']))
                            echo '<input type="idText" name="searchId" value=' . $_GET['identification'] . '>';
                        else
                            echo '<input type="idText" name="searchId" value="%" >';
                    ?>
                <a href="qr.php"><img src="qricon.png" alt="Scan" style="height:1em"></a>
                </p>  
                <p> 
                <label for="locationText">Where: </label>
                <input type="locationText" name="searchLoc" value="%">
                </p> 
                <p> 
                <label for="ownerText">Owner: </label>
                <input type="ownerText" name="searchOwner" value="%">
                </p>  
                <p>          
                <button type="submit" name="searchForm" value="Search">Search</button>
                </p>
                
                <div class="scrollitem" contenteditable="true" style="overflow-y: scroll; width:300px; height:600px; background:white; font-size: 10pt"><?php
                    if (isset($_POST['addForm']) or  isset($_POST['measurementForm']) or isset($_POST['searchForm'])) {
                        $servername = getenv('MYSQL_SERVICE_HOST');
                        $username = getenv('MYSQL_USER');
                        $password = getenv('MYSQL_PASSWORD');
                        $port = getenv('MYSQL_SERVICE_PORT');
                        $dbname = getenv('MYSQL_DATABASE');
                        
                        // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname, $port);

                        if($conn->connect_error){
                            echo 'Connection Faild: '.$conn->connect_error;
                            }
                        else
                        {
                            if (isset($_POST['searchForm']))
                            {
                                $searchId = trim($_POST['searchId']);
                                // echo $searchId . '</br>';
                                $searchLoc = $_POST['searchLoc'];
                                $searchOwner = $_POST['searchOwner'];
                                $additionalSelection = "";
                                // echo $_POST['searchType'] . '</br>';
                                if ($_POST['searchType'] == 'actions')
                                    $additionalSelection = " AND type LIKE '" . $_POST['actionType'] . "'";
                                if ($_POST['searchType'] == 'equipment')
                                    $additionalSelection = " AND owner LIKE '" . $searchOwner . "'";
                                $sql="SELECT * FROM " . $_POST['searchType'] . " WHERE identification LIKE '" . $searchId ."' AND location LIKE '". $searchLoc . "'". $additionalSelection .";";
                                // echo $sql . '</br>';
                                if ($_POST['searchType'] == 'actions')
                                    echo "Looking for ". $_POST['actionType'] . " results for '"   . $searchId . "' at '" . $searchLoc . "': </br>"; 
                                else
                                    echo "Looking for '". $searchId . "' at '" . $searchLoc . "': </br>";  
                                $result = $conn->query($sql);

                                
                                // echo $result->num_rows . '</br></br>';
                    
                                if ($result->num_rows > 0)
                                {
                                    // output data of each row
                                    while($row = $result->fetch_assoc())
                                    {
                                        if ($_POST['searchType'] == 'equipment')
                                        {
                                            echo $row["id"]. ": ";
                                            echo $row["identification"]. " is a ";
                                            if ($row['quantity']>1) 
                                                echo 'box of ' . $row['quantity'] . ' ';
                                            echo $row["description"]. " produced by " . $row["manufacturer"]. ", owned by " . $row["owner"].": at " . $row["location"]. " under the responsibility of " . $row["responsible"]. " [" . $row["email"]. "] ". "</br>";
                                            echo "Last modification: " . $row["lastmod"] . "</br>";
                                            if (isset($row["notes"]))
                                                echo "Notes: " . $row["notes"]  . "</br>";
                                        }
                                        else
                                        {
                                            echo $row["id"]. ": " ;
                                            echo $row["identification"]. ": " . $row["description"] . " performed on " . $row["identification"]. ": at " . $row["location"]. " by " . $row["responsible"]. " [" . $row["email"]. "] ";
                                            if (isset($row["filename"])) {
                                                echo "Attached file: " . $row["filename"] . " of " . $row["filesize"] . " KB". "</br>";
                                                echo '<div contentEditable="false"><a href="https://etldb.web.cern.ch/retrieveFile.php?id='. $row["id"] .'" target="_blank">Download</a></div>';
                                            }
                                            echo "Last modification: " . $row["lastmod"] . "</br>";
                                            if (isset($row["notes"]))
                                                echo "Notes: " . $row["notes"]  . "</br>";
                                        }
                                            echo "</br> </br>";
                                    }
                                }
                                else {
                                    echo "No results </br>";
                                }    
                            } 
                        }
                        // ob_clean();
                        // flush();
                        $conn->close();
                    }
                ?></div>

                </form>
            </div>
        </div>  
    </main>

    <footer>
      <p>Created by Nicola</p>
    </footer>

  </body>
</html>



